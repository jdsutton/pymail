#!/usr/bin/env python3

import smtplib
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders

class PyMail:
    '''
    Sends email.
    '''

    class Email:
        '''
        An email.
        '''

        def __init__(self, *, sender, to, message, subject=''):
            if not isinstance(to, list):
                to = [to]

            self.sender = sender
            self.recipients = to

            self.message = MIMEMultipart()
            self.message['Subject'] = subject
            self.message['From'] = sender
            self.message['To'] = ', '.join(to)

            self.message.attach(MIMEText(message, 'html'))

        def attachFile(self, f, fileName=None):
            if isinstance(f, str):
                f = open(f, 'rb')

            if fileName is None:
                fileName = filePath

            file = MIMEBase('application', 'octet-stream')
            file.set_payload(f.read())
            encoders.encode_base64(file)
            file.add_header('Content-Disposition', 'attachment', filename=fileName) 

            self.message.attach(file)

            return self

    def __init__(self, host):
        self._host = host
        self._smtp = smtplib.SMTP_SSL(self._host, 465)

    def authenticate(self, username, password):
        self._smtp.ehlo()
        self._smtp.login(username, password)

        return self

    def send(self, email):
        self._smtp.sendmail(
            email.sender,
            email.recipients,
            email.message.as_string(),
        )

        return self

    def close(self):
        self._smtp.close()

if __name__ == '__main__':
    orgName = 'Example Org'
    orgEmail = 'example@email.com'
    subject = 'Your Bill From {}'.format(orgName)
    message = '''
        Here is your bill from {orgName}.

        This message was sent automatically at the request of {orgName}. If you do not wish to receive these emails, contact {orgEmail} to unsubscribe.


        For questions or help with <a href="https://quickbillin.com">QuickBillin</a>, simply reply to this email.
    '''.format(orgName=orgName, orgEmail=orgEmail)

    email = PyMail.Email(
        sender='bdp.inbox@gmail.com',
        to='bdp.inbox@gmail.com',
        message=message,
        subject=subject,
    ).attachFile('out.png')

    user = input('Enter username: ')
    password = input('Enter password: ')

    PyMail('smtp.gmail.com') \
        .authenticate(user, password) \
        .send(email) \
        .close()
